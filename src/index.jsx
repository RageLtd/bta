import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';

import { Home } from 'views';
import routes from 'routes';
import * as reducers from 'reducers';
import { history, router } from 'util';

const container = document.getElementById('app');
/* eslint-disable no-underscore-dangle */
const store = createStore(
  combineReducers(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  ),
);
/* eslint-enable */

function renderComponent(component) {
  ReactDOM.render(<Provider store={store}>{component}</Provider>, container);
}

function render(location, children) {
  renderComponent(router.resolve(routes, location, children));
}

render(history.location, (<Home />)); // render the current URL
history.listen(render); // render subsequent URLs

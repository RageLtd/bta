import history from './history';
import * as router from './router';

export {
  history,
  router,
};

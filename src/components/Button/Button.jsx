import React, { PropTypes } from 'react';
import classNames from 'classnames';

export default function Button(props) {
  const {
    children,
    disabled,
    onClick,
    size,
    submit,
    style,
  } = props;

  const styles = classNames({
    [`button ${size} ${style}`]: true,
    disabled,
  });

  return (
    <button
      type={submit ? 'submit' : 'button'}
      disabled={disabled}
      onClick={onClick}
      className={styles}
    >
      {children}
    </button>
  );
}

Button.propTypes = {
  children: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  size: PropTypes.oneOf(['small', 'large', 'medium']),
  submit: PropTypes.bool,
  style: PropTypes.oneOf(['primary', 'secondary']),
};

Button.defaultProps = {
  disabled: false,
  size: 'medium',
  submit: false,
  style: 'primary',
};

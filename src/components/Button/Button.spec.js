/* eslint-disable */
import React from 'react';
import renderer from 'react-test-renderer';
import Button from './Button';

it('Renders', () => {
  const tree = renderer.create(<Button onClick={() => {}}>Button</Button>).toJSON();
  expect(tree).toMatchSnapshot();
})

it('Renders disabled', () => {
  const tree = renderer.create(<Button onClick={() => {}} disabled>Button</Button>).toJSON();
  expect(tree).toMatchSnapshot();
})
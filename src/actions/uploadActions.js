export const TYPES = {
  FILE_RECIEVED: 'fileRecieved',
};

export function fileRecieved(event) {
  const { files } = event.target;
  return {
    type: TYPES.FILE_RECIEVED,
    payload: {
      files,
    },
  };
}

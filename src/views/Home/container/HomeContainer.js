import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { uploadActions, actionUtils } from 'actions';
import Home from '../Home';

const mapDispatchToProps = (dispatch) => {
  const actions = actionUtils.getActions(uploadActions);
  const { fileRecieved } = actions;
  return {
    fileRecieved: bindActionCreators(fileRecieved, dispatch),
  };
};

export default connect(undefined, mapDispatchToProps)(Home);
